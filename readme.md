### The 'callbook' application is written in Java 8 with Spring Boot framework and already contains embedded tomcat-container. ###
Callbook is based on REST-architecture. The application has integration and unit-tests.
### quickstart: ###

1. specify the folder for generated files.
    * a) go to src-main-resources-application.properties
    * b) change the value of "app.fileFolder" to your.
    * c) also you can specify another tomcat port: "server.port" property.
2. build the application with maven: mvn clean package. Spring Boot will generate a runnable jar. file
3. start application: mvn spring-boot:run or you can execute: java -jar target/callbook-1.0.jar
4. if you need .war file instead .jar you should execute next simple steps: https://docs.spring.io/spring-boot/docs/current/reference/html/howto-traditional-deployment.html
5. make POST requests on http://localhost:8080/calls with next JSON-payload

```
#!javascript

{
"firstName" : "firstName",
"lastName" : "lastName",
"phoneNumber" : "+(420)-111222333"
}
```

If you input correct data server return true, if not - you will see an appropriate error message