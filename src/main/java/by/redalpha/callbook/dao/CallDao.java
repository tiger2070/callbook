package by.redalpha.callbook.dao;

import by.redalpha.callbook.domain.Call;

public interface CallDao {
    void save(Call call);
}
