package by.redalpha.callbook.dao;

import by.redalpha.callbook.api.exception.BusinessException;
import by.redalpha.callbook.domain.Call;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.time.format.DateTimeFormatter;

@Component
public class CallDaoImpl implements CallDao {
    private static final Logger LOGGER = LoggerFactory.getLogger(CallDaoImpl.class);
    private static final String FILE_EXTENSION = ".txt";
    private final String fileFolder;

    @Autowired
    public CallDaoImpl(@Value("${app.fileFolder}") String fileFolder) {
        this.fileFolder = fileFolder;
    }

    @Override
    public void save(Call call) {
        String record = generateRecord(call);
        try {
            String filePath = generateFilePath(call.getFirstName(), call.getLastName());
            Files.write(Paths.get(filePath), record.getBytes());
        } catch (IOException e) {
            throw new BusinessException("IOException occurred while writing call to disk.");
        }
    }

    private String generateRecord(Call call) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("HH:mm:ss");
        return call.getPhoneNumber() + "\r\n" + call.getTime().format(formatter);
    }

    private String generateFilePath(String firstName, String lastName) {
        return fileFolder + lastName.toUpperCase() + "_" + firstName.toUpperCase() + FILE_EXTENSION;
    }
}
