package by.redalpha.callbook.api.controller;

import by.redalpha.callbook.api.response.Response;
import by.redalpha.callbook.api.validation.CreateCallRequestValidator;
import by.redalpha.callbook.domain.Call;
import by.redalpha.callbook.service.CallService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/calls")
public class CallController {
    private static final Logger LOGGER = LoggerFactory.getLogger(CallController.class);
    @Autowired
    private CreateCallRequestValidator callValidator;
    @Autowired
    private CallService callService;

    @RequestMapping(method = RequestMethod.POST, consumes = "application/json")
    public Response create(@RequestBody Call requestCall) {
        LOGGER.debug("Received request to create the {}", requestCall);
        callValidator.validate(requestCall);
        callService.create(requestCall);
        return Response.createSuccessResponse(true);
    }

}