package by.redalpha.callbook.api.response;

import java.util.HashMap;

public final class Response extends HashMap<String, Object> {
    private static final String DATA = "data";
    private static final String ERROR_MESSAGE = "error_message";

    private Response() {
    }

    public static Response createSuccessResponse(Object data) {
        Response response = new Response();
        response.put(DATA, data);
        return response;
    }

    public static Response createErrorResponse(String errorMessage) {
        Response response = new Response();
        response.put(ERROR_MESSAGE, errorMessage);
        return response;
    }
}