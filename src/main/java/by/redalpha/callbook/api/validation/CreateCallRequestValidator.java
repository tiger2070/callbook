package by.redalpha.callbook.api.validation;

import by.redalpha.callbook.api.exception.IncorrectNameException;
import by.redalpha.callbook.api.exception.IncorrectPhoneNumberException;
import by.redalpha.callbook.domain.Call;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Component;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Component
public class CreateCallRequestValidator {
    public static final int FIRST_NAME_MAX_LENGTH = 30;
    public static final int LAST_NAME_MAX_LENGTH = 30;
    public static final int LOCAL_PART_LENGTH = 9;
    public static final int INTERNATIONAL_PART_LENGTH = 5;

    public static final String NON_DIGITS_REGEX = "[^\\d]";
    public static final String ALLOWED_CHARACTERS_REGEX = "(\\d|-|\\)|\\(|\\+|\\s)+";

    public void validate(Call call) {
        validateFirstName(call.getFirstName());
        validateLastName(call.getLastName());
        validatePhoneNumber(call.getPhoneNumber());
    }


    private void validateFirstName(String firstName) {
        if (StringUtils.isNotBlank(firstName)) {
            if (firstName.length() > FIRST_NAME_MAX_LENGTH) {
                throw new IncorrectNameException("First name exceeds max allowed length");
            }
        }
    }

    private void validateLastName(String lastName) {
        if (StringUtils.isBlank(lastName)) {
            throw new IncorrectNameException("Last name is blank. Please fill it");
        }
        if (lastName.length() > LAST_NAME_MAX_LENGTH) {
            throw new IncorrectNameException("Last name exceeds max allowed length");
        }
    }

    private void validatePhoneNumber(String phoneNumber) {
        if (StringUtils.isBlank(phoneNumber)) {
            throw new IncorrectPhoneNumberException("Phone number is blank. Please fill it");
        }
        Pattern pattern = Pattern.compile(ALLOWED_CHARACTERS_REGEX);
        Matcher matcher = pattern.matcher(phoneNumber);
        if (!matcher.matches()) {
            throw new IncorrectPhoneNumberException("Phone number contains now allowed characters");
        }
        if (phoneNumber.substring(1).contains("+")) {
            throw new IncorrectPhoneNumberException("Phone number can have + only at the beginning");
        }
        if (StringUtils.countMatches(phoneNumber, ')') != StringUtils.countMatches(phoneNumber, '(')) {
            throw new IncorrectPhoneNumberException("Phone number can not have different count of start and end brackets");
        }

        String cleanDigitNumber = phoneNumber.replaceAll(NON_DIGITS_REGEX, "");
        if (cleanDigitNumber.length() < LOCAL_PART_LENGTH) {
            throw new IncorrectPhoneNumberException("Phone number must be at least " + LOCAL_PART_LENGTH + " digits");
        }

        int maxDigitsLength = LOCAL_PART_LENGTH + INTERNATIONAL_PART_LENGTH;
        if (cleanDigitNumber.length() > maxDigitsLength) {
            throw new IncorrectPhoneNumberException("Phone number must be max " + maxDigitsLength + " digits");
        }
    }
}
