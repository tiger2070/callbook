package by.redalpha.callbook.api.exception;

public class IncorrectPhoneNumberException extends RuntimeException {
    public IncorrectPhoneNumberException() {
    }

    public IncorrectPhoneNumberException(String message) {
        super(message);
    }

    public IncorrectPhoneNumberException(String message, Throwable cause) {
        super(message, cause);
    }

    public IncorrectPhoneNumberException(Throwable cause) {
        super(cause);
    }

    public IncorrectPhoneNumberException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
