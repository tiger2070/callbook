package by.redalpha.callbook.api.exception;

import by.redalpha.callbook.api.response.Response;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

@ControllerAdvice
public class ExceptionHandler {
    private static final Logger LOGGER = LoggerFactory.getLogger(ExceptionHandler.class);

    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @org.springframework.web.bind.annotation.ExceptionHandler(IncorrectNameException.class)
    @ResponseBody
    public Response incorrectCallException(IncorrectNameException e) {
        LOGGER.debug("Exception occurred: " + e.getMessage());
        return Response.createErrorResponse(e.getMessage());
    }

    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @org.springframework.web.bind.annotation.ExceptionHandler(IncorrectPhoneNumberException.class)
    @ResponseBody
    public Response incorrectPhoneNumberException(IncorrectPhoneNumberException e) {
        LOGGER.debug("Exception occurred: " + e.getMessage());
        return Response.createErrorResponse(e.getMessage());
    }

    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    @org.springframework.web.bind.annotation.ExceptionHandler(Exception.class)
    @ResponseBody
    public Response exception(Exception e) {
        LOGGER.debug("Exception occurred: " + e.getMessage());
        return Response.createErrorResponse(e.getMessage());
    }
}
