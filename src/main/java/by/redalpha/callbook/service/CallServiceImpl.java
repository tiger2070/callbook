package by.redalpha.callbook.service;

import by.redalpha.callbook.api.validation.CreateCallRequestValidator;
import by.redalpha.callbook.dao.CallDao;
import by.redalpha.callbook.domain.Call;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;

@Service
public class CallServiceImpl implements CallService {
    private static final int INTERNATIONAL_CODE = 420;
    private static final String ZEROS = "00";

    @Autowired
    private CallDao callDao;

    @Override
    public void create(Call call) {
        prepareCall(call);
        String normalizedPhoneNumber = normalizePhoneNumber(call.getPhoneNumber());
        call.setPhoneNumber(normalizedPhoneNumber);
        callDao.save(call);
    }

    private void prepareCall(Call call) {
        call.setTime(LocalDateTime.now());
        if (StringUtils.isNotBlank(call.getFirstName())) {
            call.setFirstName(call.getFirstName().trim());
        }
        call.setLastName(call.getLastName().trim());
    }

    private String normalizePhoneNumber(String phoneNumber) {
        String resultNumber;
        String onlyDigitsNumber = phoneNumber.replaceAll(CreateCallRequestValidator.NON_DIGITS_REGEX, "");
        if (onlyDigitsNumber.length() == CreateCallRequestValidator.LOCAL_PART_LENGTH) {
            String withWhitespaces = onlyDigitsNumber.replaceAll(".{3}", "$0 ");
            resultNumber = ZEROS + INTERNATIONAL_CODE + " " + withWhitespaces;
        } else {
            String withWhitespaces = onlyDigitsNumber.replaceAll(".{3}", "$0 ");
            resultNumber = ZEROS + withWhitespaces;
        }
        return resultNumber;
    }
}
