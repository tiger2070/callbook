package by.redalpha.callbook.service;

import by.redalpha.callbook.domain.Call;

public interface CallService {
    void create(Call call);
}
