package by.redalpha.callbook.service;

import by.redalpha.callbook.dao.CallDao;
import by.redalpha.callbook.domain.Call;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.test.util.ReflectionTestUtils;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

@RunWith(MockitoJUnitRunner.class)
public class CallServiceTest {
    @Mock
    private CallDao callDao;
    private CallService callService;

    @Before
    public void setup() {
        callService = new CallServiceImpl();
        ReflectionTestUtils.setField(callService, "callDao", callDao);
    }

    @Test
    public void createTest() {
        Call call = new Call("firstName","lastName", "+(420)-111222333");
        callService.create(call);
        // verify call was passed to CallDao
        verify(callDao, times(1)).save(call);

    }
}
