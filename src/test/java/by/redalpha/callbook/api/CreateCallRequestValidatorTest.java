package by.redalpha.callbook.api;


import by.redalpha.callbook.api.exception.IncorrectNameException;
import by.redalpha.callbook.api.exception.IncorrectPhoneNumberException;
import by.redalpha.callbook.api.validation.CreateCallRequestValidator;
import by.redalpha.callbook.domain.Call;
import junitparams.JUnitParamsRunner;
import junitparams.Parameters;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import static junitparams.JUnitParamsRunner.$;

@RunWith(JUnitParamsRunner.class)
public class CreateCallRequestValidatorTest {

    private CreateCallRequestValidator callValidator;

    @Before
    public void setup() {
        callValidator = new CreateCallRequestValidator();
    }

    @Test
    @Parameters(method = "createValidCallsForValidateCallTest")
    public void validateTest(Call call) {
        callValidator.validate(call);

    }

    public Object[] createValidCallsForValidateCallTest() {
        return $(
                $(new Call("firstname","lastname", "+(420) 111 222 333")),
                $(new Call(null,"lastname", "+(420)-111222333")),
                $(new Call("firstname","lastname", "+420111222333")),
                $(new Call("firstname","lastname", "+00420111222333")),
                $(new Call("firstname","lastname", "+00420111222333")),
                $(new Call("firstname","lastname", "00420111222333")),
                $(new Call("firstname","lastname", "(111) 222 (333)")),
                $(new Call("firstname","lastname", "123456789"))
        );
    }

    @Test(expected = IncorrectNameException.class)
    @Parameters(method = "createDataForIncorrectNameException")
    public void incorrectNameExceptionTest(Call call) {
        callValidator.validate(call);

    }

    public Object[] createDataForIncorrectNameException() {
        return $(
                $(new Call("firstname","", "(111) 222 (333)")),
                $(new Call("firstname","   ", "123456789")),
                $(new Call("","   ", "123456789")),
                $(new Call("","abcdefghijklmnopqrstuvwxyzabcdef", "123456789"))
        );
    }

    @Test(expected = IncorrectPhoneNumberException.class)
    @Parameters(method = "createDataForIncorrectPhoneNumberException")
    public void incorrectPhoneNumberExceptionTest(Call call) {
        callValidator.validate(call);

    }

    public Object[] createDataForIncorrectPhoneNumberException() {
        return $(
                $(new Call("firstname","lastname", "   ")),
                $(new Call("firstname","lastname", "((111) 222 (333)")),
                $(new Call("firstname","lastname", "+4201+11222333")),
                $(new Call("firstname","lastname", "12345678")),
                $(new Call("firstname","lastname", "123456789012345")),
                $(new Call("firstname","lastname", "asd31231312313"))
        );
    }


}
