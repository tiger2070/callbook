package by.redalpha.callbook.api;

import by.redalpha.callbook.api.controller.CallController;
import by.redalpha.callbook.api.validation.CreateCallRequestValidator;
import by.redalpha.callbook.domain.Call;
import by.redalpha.callbook.service.CallService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.test.util.ReflectionTestUtils;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

@RunWith(MockitoJUnitRunner.class)
public class CallControllerTest {
    private CallController callController;
    @Mock
    private CallService callService;
    @Mock
    private CreateCallRequestValidator validator;

    @Before
    public void setUp() {
        callController = new CallController();
        ReflectionTestUtils.setField(callController, "callService", callService);
        ReflectionTestUtils.setField(callController, "callValidator", validator);
    }

    @Test
    public void createCallTest() {
        Call call = new Call();
        callController.create(call);
        // verify call was passed to CallService
        verify(callService, times(1)).create(call);
    }


}
